#pragma once

#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <fstream>
#include <list>
#include <algorithm> 
#include <msclr\marshal_cppstd.h>
#include <C:/Users/mmajak/Documents/Visual studio 2013/Projects/libgit2test/libgit2test/GitProjectInfo.h>
#include <sstream>      // std::istringstream

using namespace std;

namespace libgit2grid {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Xml;

	GitProjectInfo info;
	vector <int> to_fetch;
	vector<string> l_paths, last_searched, tags, branches, csv_paths;
	int loaded = -1;
	map <string, PackageInfo> data;
	string local_path, tag_from_textb, path_to_clone, path_to_csv, tag_from_csv, version, absolute_path, save_csv_there, create_repositories_there, remote, relative_path_first, origin_local_path, folder_path;
	int version_counter = 1;
	int can_continue = 0;
	int compared = 0;
	string commit_message = "Commit created by button, message box empty";

	map <string, PackageInfo> to_clone = map <string, PackageInfo>();
	multimap <string, PackageInfo> tag_to_switch = multimap <string, PackageInfo>();
	map <string, PackageInfo> to_delete = map <string, PackageInfo>();
	map <string, PackageInfo> ok_files = map <string, PackageInfo>();
	multimap <string, PackageInfo> csv_data = multimap <string, PackageInfo>();

	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			deleteMoreThanTenLPaths();
			loadLocalPaths();

			//
			//TODO: Add the constructor code here
			//
		}

		// delete items from xml file, if there are more than 10 different
	private: void deleteMoreThanTenLPaths(){
		int counter = 0;
		int counter2 = 0;
		try{
			XmlDocument^ doc = gcnew XmlDocument();
			doc->Load("Local_paths.xml");

			for each (XmlNode^ node in doc->DocumentElement) counter++;
			if (counter <= 10) {}
			else{
				counter -= 10;
				for each (XmlNode^ node in doc->SelectNodes("root/LocalPath")) {
					counter2++;

					node->ParentNode->RemoveChild(node);
					doc->Save("Local_paths.xml");

					if (counter2 == counter) break;
				}
			}
			delete doc;
		}
		catch (...){
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Not able to open Local_paths.xml file." << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

			 // load local paths from xml to combobox, called in constructor
	private: void loadLocalPaths() {
		int counter = 0;
		try{
			XmlDocument^ doc = gcnew XmlDocument();
			doc->Load("Local_paths.xml");

			for each (XmlNode^ node in doc->SelectNodes("root/LocalPath"))
			{

				counter++;
				String ^ ll_path = node->LastChild->InnerText;
				using namespace msclr::interop;
				std::string loc_path(marshal_as<std::string>(ll_path));
				fillLastSearched(loc_path);
				addToComboBox();

				if (counter >= 10) break;
			}
			delete doc;
		}
		catch (...){
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Not able to open Local_paths.xml file." << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

			 // add sent local_path to xml, check if it already is in xml -> if yes, it's being deleted and also added as the last child (it will be in first place when you re-open application)
	private: void addToXml(string local_path){
		try{
			XmlDocument^ doc1 = gcnew XmlDocument();
			doc1->Load("Local_paths.xml");

			String^ str = gcnew String(local_path.c_str());

			for each (XmlNode^ node in doc1->SelectNodes("root/LocalPath"))
			{
				if (node->LastChild->InnerText == str) {
					node->ParentNode->RemoveChild(node);
					doc1->Save("Local_paths.xml");
					break;
				}
			}

			XmlNode^ lpath = doc1->CreateElement("LocalPath");

			lpath->InnerText = str;
			doc1->DocumentElement->AppendChild(lpath);


			doc1->Save("Local_paths.xml");
		}
		catch (...){
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Not able to open Local_paths.xml file." << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	protected:

	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;

	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Button^  button7;
	private: System::Windows::Forms::ComboBox^  comboBox1;
	private: System::Windows::Forms::Button^  button8;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Button^  button9;
	private: System::Windows::Forms::Button^  button10;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Button^  button11;
	private: System::Windows::Forms::Button^  button12;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::Button^  button13;

	private: System::Windows::Forms::ComboBox^  comboBox2;

	private: System::Windows::Forms::Button^  button14;

	private: System::Windows::Forms::Button^  button15;
	private: System::Windows::Forms::TextBox^  textBox7;
	private: System::Windows::Forms::Button^  button16;

	private: System::Windows::Forms::Button^  button17;
	private: System::Windows::Forms::Button^  button18;
	private: System::Windows::Forms::Button^  button19;
	private: System::Windows::Forms::Button^  button20;
	private: System::Windows::Forms::Button^  button21;
	private: System::Windows::Forms::TextBox^  textBox8;
	private: System::Windows::Forms::Button^  button22;
	private: System::Windows::Forms::TextBox^  textBox9;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::GroupBox^  groupBox3;
	private: System::Windows::Forms::GroupBox^  groupBox4;
	private: System::Windows::Forms::GroupBox^  groupBox5;
	private: System::Windows::Forms::GroupBox^  groupBox6;
	private: System::Windows::Forms::GroupBox^  groupBox7;
	private: System::Windows::Forms::GroupBox^  groupBox8;
	private: System::Windows::Forms::GroupBox^  groupBox9;
	private: System::Windows::Forms::TextBox^  textBox6;
	private: System::Windows::Forms::DataGridViewCheckBoxColumn^  Column4;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column2;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column5;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column3;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column6;
	private: System::Windows::Forms::RichTextBox^  richTextBox1;
	private: System::Windows::Forms::Button^  button23;
	private: System::Windows::Forms::Button^  button24;
	private: System::Windows::Forms::Button^  button26;
	private: System::Windows::Forms::Button^  button25;
	private: System::Windows::Forms::TextBox^  textBox5;
	private: System::Windows::Forms::Button^  button27;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>

		void InitializeComponent(void)
		{
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->Column4 = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
			this->Column1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column5 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column6 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->button10 = (gcnew System::Windows::Forms::Button());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->button11 = (gcnew System::Windows::Forms::Button());
			this->button12 = (gcnew System::Windows::Forms::Button());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->button13 = (gcnew System::Windows::Forms::Button());
			this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
			this->button14 = (gcnew System::Windows::Forms::Button());
			this->button15 = (gcnew System::Windows::Forms::Button());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->button16 = (gcnew System::Windows::Forms::Button());
			this->button17 = (gcnew System::Windows::Forms::Button());
			this->button18 = (gcnew System::Windows::Forms::Button());
			this->button19 = (gcnew System::Windows::Forms::Button());
			this->button20 = (gcnew System::Windows::Forms::Button());
			this->button21 = (gcnew System::Windows::Forms::Button());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->button22 = (gcnew System::Windows::Forms::Button());
			this->textBox9 = (gcnew System::Windows::Forms::TextBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->button27 = (gcnew System::Windows::Forms::Button());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->button26 = (gcnew System::Windows::Forms::Button());
			this->button25 = (gcnew System::Windows::Forms::Button());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox6 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox7 = (gcnew System::Windows::Forms::GroupBox());
			this->button24 = (gcnew System::Windows::Forms::Button());
			this->button23 = (gcnew System::Windows::Forms::Button());
			this->groupBox8 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox9 = (gcnew System::Windows::Forms::GroupBox());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->groupBox6->SuspendLayout();
			this->groupBox7->SuspendLayout();
			this->groupBox8->SuspendLayout();
			this->groupBox9->SuspendLayout();
			this->SuspendLayout();
			// 
			// dataGridView1
			// 
			this->dataGridView1->BackgroundColor = System::Drawing::SystemColors::ActiveCaption;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {
				this->Column4,
					this->Column1, this->Column2, this->Column5, this->Column3, this->Column6
			});
			this->dataGridView1->EditMode = System::Windows::Forms::DataGridViewEditMode::EditOnEnter;
			this->dataGridView1->Location = System::Drawing::Point(332, 81);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->Size = System::Drawing::Size(1143, 527);
			this->dataGridView1->TabIndex = 0;
			this->dataGridView1->CellContentClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MyForm::dataGridView1_CellContentClick);
			// 
			// Column4
			// 
			this->Column4->HeaderText = L"Select";
			this->Column4->Name = L"Column4";
			this->Column4->Resizable = System::Windows::Forms::DataGridViewTriState::True;
			this->Column4->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Automatic;
			this->Column4->Width = 40;
			// 
			// Column1
			// 
			this->Column1->HeaderText = L"Local path";
			this->Column1->Name = L"Column1";
			this->Column1->Width = 280;
			// 
			// Column2
			// 
			this->Column2->HeaderText = L"Remote URL";
			this->Column2->Name = L"Column2";
			this->Column2->Width = 260;
			// 
			// Column5
			// 
			this->Column5->HeaderText = L"Current tag/branch";
			this->Column5->Name = L"Column5";
			this->Column5->Width = 125;
			// 
			// Column3
			// 
			this->Column3->HeaderText = L"Version";
			this->Column3->Name = L"Column3";
			this->Column3->Width = 250;
			// 
			// Column6
			// 
			this->Column6->HeaderText = L"All changes committed\?";
			this->Column6->Name = L"Column6";
			this->Column6->Width = 145;
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button1->Location = System::Drawing::Point(6, 107);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(80, 36);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Load";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button2->Location = System::Drawing::Point(244, 107);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(77, 36);
			this->button2->TabIndex = 2;
			this->button2->Text = L"Fetch";
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// button3
			// 
			this->button3->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button3->Location = System::Drawing::Point(216, 15);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(98, 46);
			this->button3->TabIndex = 3;
			this->button3->Text = L"Output to .csv file";
			this->button3->UseVisualStyleBackColor = false;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
			// 
			// button4
			// 
			this->button4->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button4->Location = System::Drawing::Point(92, 107);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(73, 36);
			this->button4->TabIndex = 4;
			this->button4->Text = L"Select all";
			this->button4->UseVisualStyleBackColor = false;
			this->button4->Click += gcnew System::EventHandler(this, &MyForm::button4_Click);
			// 
			// button5
			// 
			this->button5->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button5->Location = System::Drawing::Point(165, 107);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(73, 36);
			this->button5->TabIndex = 5;
			this->button5->Text = L"Discard all";
			this->button5->UseVisualStyleBackColor = false;
			this->button5->Click += gcnew System::EventHandler(this, &MyForm::button5_Click);
			// 
			// button6
			// 
			this->button6->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button6->Location = System::Drawing::Point(6, 19);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(315, 38);
			this->button6->TabIndex = 6;
			this->button6->Text = L"Select folder with repositories";
			this->button6->UseVisualStyleBackColor = false;
			this->button6->Click += gcnew System::EventHandler(this, &MyForm::button6_Click);
			// 
			// textBox1
			// 
			this->textBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox1->Location = System::Drawing::Point(6, 81);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(315, 20);
			this->textBox1->TabIndex = 7;
			this->textBox1->Text = L"No folder selected";
			this->textBox1->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBox1->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox1_TextChanged);
			// 
			// button7
			// 
			this->button7->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button7->Location = System::Drawing::Point(3, 29);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(73, 32);
			this->button7->TabIndex = 8;
			this->button7->Text = L"Clear";
			this->button7->UseVisualStyleBackColor = false;
			this->button7->Click += gcnew System::EventHandler(this, &MyForm::button7_Click);
			// 
			// comboBox1
			// 
			this->comboBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Location = System::Drawing::Point(6, 54);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(315, 21);
			this->comboBox1->TabIndex = 9;
			this->comboBox1->Text = L"                                             Last searched";
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::comboBox1_SelectedIndexChanged);
			// 
			// button8
			// 
			this->button8->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button8->Location = System::Drawing::Point(6, 99);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(249, 36);
			this->button8->TabIndex = 10;
			this->button8->Text = L"Select where to clone repositories from csv file";
			this->button8->UseVisualStyleBackColor = false;
			this->button8->Click += gcnew System::EventHandler(this, &MyForm::button8_Click);
			// 
			// textBox2
			// 
			this->textBox2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox2->Location = System::Drawing::Point(6, 132);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(249, 20);
			this->textBox2->TabIndex = 11;
			this->textBox2->Text = L"No folder selected";
			this->textBox2->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBox2->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox2_TextChanged);
			// 
			// button9
			// 
			this->button9->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button9->Location = System::Drawing::Point(261, 119);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(60, 32);
			this->button9->TabIndex = 12;
			this->button9->Text = L"Clone";
			this->button9->TextImageRelation = System::Windows::Forms::TextImageRelation::TextBeforeImage;
			this->button9->UseVisualStyleBackColor = false;
			this->button9->Click += gcnew System::EventHandler(this, &MyForm::button9_Click);
			// 
			// button10
			// 
			this->button10->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->button10->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button10->Location = System::Drawing::Point(6, 14);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(315, 28);
			this->button10->TabIndex = 13;
			this->button10->Text = L"Select csv file with data";
			this->button10->UseVisualStyleBackColor = false;
			this->button10->Click += gcnew System::EventHandler(this, &MyForm::button10_Click);
			// 
			// textBox3
			// 
			this->textBox3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox3->Location = System::Drawing::Point(6, 39);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(315, 20);
			this->textBox3->TabIndex = 14;
			this->textBox3->Text = L"No file selected";
			this->textBox3->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBox3->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox3_TextChanged);
			// 
			// button11
			// 
			this->button11->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button11->Location = System::Drawing::Point(85, 39);
			this->button11->Name = L"button11";
			this->button11->Size = System::Drawing::Size(237, 24);
			this->button11->TabIndex = 15;
			this->button11->Text = L"Create tag";
			this->button11->UseVisualStyleBackColor = false;
			this->button11->Click += gcnew System::EventHandler(this, &MyForm::button11_Click);
			// 
			// button12
			// 
			this->button12->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button12->Location = System::Drawing::Point(180, 61);
			this->button12->Name = L"button12";
			this->button12->Size = System::Drawing::Size(142, 29);
			this->button12->TabIndex = 16;
			this->button12->Text = L"Switch to tag/branch";
			this->button12->UseVisualStyleBackColor = false;
			this->button12->Click += gcnew System::EventHandler(this, &MyForm::button12_Click);
			// 
			// textBox4
			// 
			this->textBox4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox4->Location = System::Drawing::Point(85, 19);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(237, 20);
			this->textBox4->TabIndex = 17;
			this->textBox4->Text = L"Write tag here or just push button for auto-fill";
			this->textBox4->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBox4->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox4_MouseClick);
			this->textBox4->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox4_TextChanged);
			// 
			// button13
			// 
			this->button13->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button13->Location = System::Drawing::Point(7, 19);
			this->button13->Name = L"button13";
			this->button13->Size = System::Drawing::Size(72, 44);
			this->button13->TabIndex = 18;
			this->button13->Text = L"Load tags / branches";
			this->button13->UseVisualStyleBackColor = false;
			this->button13->Click += gcnew System::EventHandler(this, &MyForm::button13_Click);
			// 
			// comboBox2
			// 
			this->comboBox2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->comboBox2->FormattingEnabled = true;
			this->comboBox2->Location = System::Drawing::Point(7, 61);
			this->comboBox2->Name = L"comboBox2";
			this->comboBox2->Size = System::Drawing::Size(167, 21);
			this->comboBox2->TabIndex = 20;
			this->comboBox2->Text = L"      No tags/branches loaded";
			this->comboBox2->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::comboBox2_SelectedIndexChanged);
			// 
			// button14
			// 
			this->button14->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button14->Location = System::Drawing::Point(6, 61);
			this->button14->Name = L"button14";
			this->button14->Size = System::Drawing::Size(221, 29);
			this->button14->TabIndex = 23;
			this->button14->Text = L"Add all changes and create commit";
			this->button14->UseVisualStyleBackColor = false;
			this->button14->Click += gcnew System::EventHandler(this, &MyForm::button14_Click);
			// 
			// button15
			// 
			this->button15->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button15->Location = System::Drawing::Point(228, 61);
			this->button15->Name = L"button15";
			this->button15->Size = System::Drawing::Size(93, 29);
			this->button15->TabIndex = 25;
			this->button15->Text = L"Push";
			this->button15->UseVisualStyleBackColor = false;
			this->button15->Click += gcnew System::EventHandler(this, &MyForm::button15_Click);
			// 
			// textBox7
			// 
			this->textBox7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox7->Location = System::Drawing::Point(6, 38);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(204, 20);
			this->textBox7->TabIndex = 26;
			this->textBox7->Text = L"No folder selected";
			this->textBox7->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBox7->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox7_TextChanged);
			// 
			// button16
			// 
			this->button16->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button16->Location = System::Drawing::Point(6, 15);
			this->button16->Name = L"button16";
			this->button16->Size = System::Drawing::Size(204, 26);
			this->button16->TabIndex = 27;
			this->button16->Text = L"Select where to save csv file";
			this->button16->UseVisualStyleBackColor = false;
			this->button16->Click += gcnew System::EventHandler(this, &MyForm::button16_Click);
			// 
			// button17
			// 
			this->button17->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button17->Location = System::Drawing::Point(180, 89);
			this->button17->Name = L"button17";
			this->button17->Size = System::Drawing::Size(142, 27);
			this->button17->TabIndex = 28;
			this->button17->Text = L"Force switch";
			this->button17->UseVisualStyleBackColor = false;
			this->button17->Click += gcnew System::EventHandler(this, &MyForm::button17_Click);
			// 
			// button18
			// 
			this->button18->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button18->Location = System::Drawing::Point(7, 88);
			this->button18->Name = L"button18";
			this->button18->Size = System::Drawing::Size(167, 28);
			this->button18->TabIndex = 29;
			this->button18->Text = L"Show conflict files";
			this->button18->UseVisualStyleBackColor = false;
			this->button18->Click += gcnew System::EventHandler(this, &MyForm::button18_Click);
			// 
			// button19
			// 
			this->button19->BackColor = System::Drawing::Color::Red;
			this->button19->Location = System::Drawing::Point(6, 14);
			this->button19->Name = L"button19";
			this->button19->Size = System::Drawing::Size(110, 44);
			this->button19->TabIndex = 30;
			this->button19->Text = L"Set credentials";
			this->button19->UseVisualStyleBackColor = false;
			this->button19->Click += gcnew System::EventHandler(this, &MyForm::button19_Click);
			// 
			// button20
			// 
			this->button20->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button20->Location = System::Drawing::Point(6, 15);
			this->button20->Name = L"button20";
			this->button20->Size = System::Drawing::Size(197, 27);
			this->button20->TabIndex = 31;
			this->button20->Text = L"Select folder without git repositories";
			this->button20->UseVisualStyleBackColor = false;
			this->button20->Click += gcnew System::EventHandler(this, &MyForm::button20_Click);
			// 
			// button21
			// 
			this->button21->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button21->Location = System::Drawing::Point(203, 15);
			this->button21->Name = L"button21";
			this->button21->Size = System::Drawing::Size(95, 44);
			this->button21->TabIndex = 32;
			this->button21->Text = L"Create git repo in each subfolder";
			this->button21->UseVisualStyleBackColor = false;
			this->button21->Click += gcnew System::EventHandler(this, &MyForm::button21_Click);
			// 
			// textBox8
			// 
			this->textBox8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox8->Location = System::Drawing::Point(6, 38);
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(197, 20);
			this->textBox8->TabIndex = 33;
			this->textBox8->Text = L"No folder selected";
			this->textBox8->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBox8->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox8_TextChanged);
			// 
			// button22
			// 
			this->button22->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button22->Location = System::Drawing::Point(6, 32);
			this->button22->Name = L"button22";
			this->button22->Size = System::Drawing::Size(271, 30);
			this->button22->TabIndex = 34;
			this->button22->Text = L"Add a remote";
			this->button22->UseVisualStyleBackColor = false;
			this->button22->Click += gcnew System::EventHandler(this, &MyForm::button22_Click);
			// 
			// textBox9
			// 
			this->textBox9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox9->Location = System::Drawing::Point(6, 15);
			this->textBox9->Name = L"textBox9";
			this->textBox9->Size = System::Drawing::Size(271, 20);
			this->textBox9->TabIndex = 35;
			this->textBox9->Text = L"Write remote url here";
			this->textBox9->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBox9->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox9_MouseClick);
			this->textBox9->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox9_TextChanged);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->button27);
			this->groupBox1->Controls->Add(this->textBox5);
			this->groupBox1->Controls->Add(this->button26);
			this->groupBox1->Controls->Add(this->button25);
			this->groupBox1->Controls->Add(this->button13);
			this->groupBox1->Controls->Add(this->button17);
			this->groupBox1->Controls->Add(this->button18);
			this->groupBox1->Controls->Add(this->comboBox2);
			this->groupBox1->Controls->Add(this->textBox4);
			this->groupBox1->Controls->Add(this->button12);
			this->groupBox1->Controls->Add(this->button11);
			this->groupBox1->Location = System::Drawing::Point(2, 329);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(328, 177);
			this->groupBox1->TabIndex = 36;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"TAGS section";
			// 
			// button27
			// 
			this->button27->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button27->Location = System::Drawing::Point(150, 151);
			this->button27->Name = L"button27";
			this->button27->Size = System::Drawing::Size(172, 26);
			this->button27->TabIndex = 33;
			this->button27->Text = L"Switch all csv including projects";
			this->button27->UseVisualStyleBackColor = false;
			this->button27->Click += gcnew System::EventHandler(this, &MyForm::button27_Click);
			// 
			// textBox5
			// 
			this->textBox5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox5->Location = System::Drawing::Point(10, 151);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(134, 20);
			this->textBox5->TabIndex = 32;
			this->textBox5->Text = L"No folder selected";
			this->textBox5->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBox5->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox5_TextChanged_1);
			// 
			// button26
			// 
			this->button26->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button26->Location = System::Drawing::Point(10, 122);
			this->button26->Name = L"button26";
			this->button26->Size = System::Drawing::Size(134, 28);
			this->button26->TabIndex = 31;
			this->button26->Text = L"Select destination folder";
			this->button26->UseVisualStyleBackColor = false;
			this->button26->Click += gcnew System::EventHandler(this, &MyForm::button26_Click);
			// 
			// button25
			// 
			this->button25->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button25->Location = System::Drawing::Point(150, 122);
			this->button25->Name = L"button25";
			this->button25->Size = System::Drawing::Size(172, 28);
			this->button25->TabIndex = 30;
			this->button25->Text = L"Switch csv with all files inside";
			this->button25->UseVisualStyleBackColor = false;
			this->button25->Click += gcnew System::EventHandler(this, &MyForm::button25_Click);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->button7);
			this->groupBox2->Location = System::Drawing::Point(1387, 12);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(79, 64);
			this->groupBox2->TabIndex = 37;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Clear section";
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->button19);
			this->groupBox3->Location = System::Drawing::Point(1259, 12);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(122, 64);
			this->groupBox3->TabIndex = 38;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Credentials section";
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->button14);
			this->groupBox4->Controls->Add(this->richTextBox1);
			this->groupBox4->Controls->Add(this->button15);
			this->groupBox4->Location = System::Drawing::Point(3, 512);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(327, 96);
			this->groupBox4->TabIndex = 39;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"Commit / push section";
			this->groupBox4->Enter += gcnew System::EventHandler(this, &MyForm::groupBox4_Enter);
			// 
			// richTextBox1
			// 
			this->richTextBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->richTextBox1->Location = System::Drawing::Point(6, 18);
			this->richTextBox1->Name = L"richTextBox1";
			this->richTextBox1->Size = System::Drawing::Size(315, 38);
			this->richTextBox1->TabIndex = 45;
			this->richTextBox1->Text = L"Write commit message";
			this->richTextBox1->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::richTextBox1_MouseClick);
			this->richTextBox1->TextChanged += gcnew System::EventHandler(this, &MyForm::richTextBox1_TextChanged);
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->button16);
			this->groupBox5->Controls->Add(this->button3);
			this->groupBox5->Controls->Add(this->textBox7);
			this->groupBox5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->groupBox5->Location = System::Drawing::Point(933, 9);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(320, 67);
			this->groupBox5->TabIndex = 40;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"Save section";
			// 
			// groupBox6
			// 
			this->groupBox6->Controls->Add(this->button6);
			this->groupBox6->Controls->Add(this->button1);
			this->groupBox6->Controls->Add(this->button2);
			this->groupBox6->Controls->Add(this->button4);
			this->groupBox6->Controls->Add(this->button5);
			this->groupBox6->Controls->Add(this->textBox1);
			this->groupBox6->Controls->Add(this->comboBox1);
			this->groupBox6->Location = System::Drawing::Point(3, 172);
			this->groupBox6->Name = L"groupBox6";
			this->groupBox6->Size = System::Drawing::Size(327, 151);
			this->groupBox6->TabIndex = 41;
			this->groupBox6->TabStop = false;
			this->groupBox6->Text = L"Visual section";
			// 
			// groupBox7
			// 
			this->groupBox7->Controls->Add(this->button24);
			this->groupBox7->Controls->Add(this->button23);
			this->groupBox7->Controls->Add(this->button10);
			this->groupBox7->Controls->Add(this->button8);
			this->groupBox7->Controls->Add(this->textBox2);
			this->groupBox7->Controls->Add(this->button9);
			this->groupBox7->Controls->Add(this->textBox3);
			this->groupBox7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->groupBox7->Location = System::Drawing::Point(3, 9);
			this->groupBox7->Name = L"groupBox7";
			this->groupBox7->Size = System::Drawing::Size(327, 157);
			this->groupBox7->TabIndex = 42;
			this->groupBox7->TabStop = false;
			this->groupBox7->Text = L"Clone / compare section";
			// 
			// button24
			// 
			this->button24->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button24->Location = System::Drawing::Point(171, 64);
			this->button24->Name = L"button24";
			this->button24->Size = System::Drawing::Size(150, 29);
			this->button24->TabIndex = 16;
			this->button24->Text = L"Apply changes";
			this->button24->UseVisualStyleBackColor = false;
			this->button24->Click += gcnew System::EventHandler(this, &MyForm::button24_Click);
			// 
			// button23
			// 
			this->button23->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->button23->Location = System::Drawing::Point(6, 64);
			this->button23->Name = L"button23";
			this->button23->Size = System::Drawing::Size(159, 29);
			this->button23->TabIndex = 15;
			this->button23->Text = L"Compare csv with grid";
			this->button23->UseVisualStyleBackColor = false;
			this->button23->Click += gcnew System::EventHandler(this, &MyForm::button23_Click);
			// 
			// groupBox8
			// 
			this->groupBox8->Controls->Add(this->textBox9);
			this->groupBox8->Controls->Add(this->button22);
			this->groupBox8->Location = System::Drawing::Point(644, 9);
			this->groupBox8->Name = L"groupBox8";
			this->groupBox8->Size = System::Drawing::Size(283, 67);
			this->groupBox8->TabIndex = 43;
			this->groupBox8->TabStop = false;
			this->groupBox8->Text = L"Adding remote section";
			// 
			// groupBox9
			// 
			this->groupBox9->Controls->Add(this->button20);
			this->groupBox9->Controls->Add(this->button21);
			this->groupBox9->Controls->Add(this->textBox8);
			this->groupBox9->Location = System::Drawing::Point(334, 9);
			this->groupBox9->Name = L"groupBox9";
			this->groupBox9->Size = System::Drawing::Size(304, 67);
			this->groupBox9->TabIndex = 44;
			this->groupBox9->TabStop = false;
			this->groupBox9->Text = L"Creating repositories section";
			// 
			// textBox6
			// 
			this->textBox6->Location = System::Drawing::Point(482, 82);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(210, 20);
			this->textBox6->TabIndex = 22;
			this->textBox6->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox6_TextChanged);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1469, 608);
			this->Controls->Add(this->groupBox9);
			this->Controls->Add(this->groupBox8);
			this->Controls->Add(this->groupBox7);
			this->Controls->Add(this->groupBox6);
			this->Controls->Add(this->groupBox5);
			this->Controls->Add(this->groupBox4);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->textBox6);
			this->Controls->Add(this->dataGridView1);
			this->Name = L"MyForm";
			this->Text = L"Git Package Manager";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox3->ResumeLayout(false);
			this->groupBox4->ResumeLayout(false);
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			this->groupBox6->ResumeLayout(false);
			this->groupBox6->PerformLayout();
			this->groupBox7->ResumeLayout(false);
			this->groupBox7->PerformLayout();
			this->groupBox8->ResumeLayout(false);
			this->groupBox8->PerformLayout();
			this->groupBox9->ResumeLayout(false);
			this->groupBox9->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		// fill the rows of dataGridView according to data  
	private: void fillData() {
		int row_number = 0;
		for (auto& walker : data){

			// set relative path name
			string relative_path = walker.second.local_path;
			string relative_path_name;
			relative_path.erase(relative_path.size() - 1, 1);
			if (relative_path.size() > local_path.size()) {
				relative_path_name = relative_path.erase(0, local_path.size() + 1);
			}
			else {
				istringstream iss(relative_path);
				while (getline(iss, relative_path_name, '/')){}
			}

			// display common path to text box
			string temp = walker.second.local_path;
			string display_common_path = temp.erase(temp.size() - relative_path_name.size() - 1, relative_path_name.size() + 1);
			String ^ path_ = gcnew String(display_common_path.c_str());
			textBox6->Text = path_;

			using namespace msclr::interop;
			string path__(marshal_as<std::string>(textBox6->Text));
			relative_path_first = path__;

			// set remote repository name 
			string relative_url = walker.second.url;
			string relative_url_name;
			istringstream iss1(relative_url);
			while (getline(iss1, relative_url_name, '/')){}

			String ^ path = gcnew String(relative_path_name.c_str());
			String ^ url = gcnew String(walker.second.url.c_str());
			String ^ tag = gcnew String(walker.second.tag.c_str());
			String ^ act = gcnew String(walker.second.commit_info.c_str());
			// this->dataGridView1->Rows->Add(path, url, act);
			this->dataGridView1->Rows->Add();
			DataGridViewCheckBoxColumn ^ colFullTime = gcnew DataGridViewCheckBoxColumn;

			this->dataGridView1->Rows[row_number]->Cells[1]->Value = path;
			this->dataGridView1->Rows[row_number]->Cells[2]->Value = url;
			this->dataGridView1->Rows[row_number]->Cells[3]->Value = tag;
			this->dataGridView1->Rows[row_number]->Cells[4]->Value = act;

			/*
			// switch to version from csv (I moved that to method cloneRepository())
			if (walker.second.tag.empty() == false){
			string vers = walker.second.tag;
			if (vers != "master"){
			vers = "refs/tags/" + vers;
			}
			else vers = "refs/remotes/origin/" + vers;

			info.setToTag(walker.second.local_path, vers);
			}
			*/
			row_number++;
		}
		loaded++;
	}

			 // add selected path to comboBox with last searched local paths
	private: void addToComboBox() {
		comboBox1->Items->Clear();
		for (int i = last_searched.size() - 1; i >= 0; i--) {
			String ^ last_path = gcnew String((last_searched.at(i)).c_str());
			comboBox1->Items->Add(last_path);
		}
	}

			 // add color to each row according to changes(if all changes are committed, it's highlighted in green, if there are some uncommitted changes, it's highlighted in red)
	private: void highlightUncommitted() {
		int row_number = 0;
		for (auto& walker : data){
			if (info.gitDiff(walker.second.local_path, 1) == 0) {
				this->dataGridView1->Rows[row_number]->Cells[5]->Style->BackColor = System::Drawing::Color::Green;
				String ^ text = gcnew String("You can switch versions");
				this->dataGridView1->Rows[row_number]->Cells[5]->Value = text;
			}
			else {
				this->dataGridView1->Rows[row_number]->Cells[5]->Style->BackColor = System::Drawing::Color::Red;
				String ^ text = gcnew String("There are some uncommited changes, you can see conflict files by pushing 'Show conflict files' button");
				this->dataGridView1->Rows[row_number]->Cells[5]->Value = text;
			}
			row_number++;
		}
	}

			 // button Load
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		if (loaded == 0){
			fillLastSearched(local_path);
			addToComboBox();
			info.getPackages(local_path);
			data = info.getPackageInfoMap();
			if (info.getCounter() > 0){
				fillData();
				highlightUncommitted();
			}
			else {
				String ^ message = gcnew String("Given path do not include .git repositories");
				this->dataGridView1->Rows[0]->Cells[1]->Value = message;
			}
		}
		else {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select some folder" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		// addToComboBox();
	}

			 // fill l_paths from data
	private: void fillLPaths() {
		int counter = 0;
		// saving local paths of checked rows
		for (auto& walker : data){
			for (unsigned int i = 0; i < to_fetch.size(); i++){
				if (counter == to_fetch.at(i)) {
					if (std::find(l_paths.begin(), l_paths.end(), walker.second.local_path) == l_paths.end()) {
						l_paths.push_back(walker.second.local_path);
					}
				}
			}
			counter++;
		}
	}

			 // add color to each row according to actuality
	private: void highlightRows() {
		int row_number = 0;
		int i = 0;
		for (auto& walker : data){
			if (find(to_fetch.begin(), to_fetch.end(), row_number) != to_fetch.end()) {
				if (walker.second.commit_number == 0){
					for (int j = 1; j <= 3; j++){
						//this->dataGridView1->Rows[row_number]->Cells[j]->Style->BackColor = System::Drawing::Color::Green;
					}
					DataGridViewRow ^dataGridViewRow = this->dataGridView1->Rows[row_number];
					dataGridViewRow->HeaderCell->Style->BackColor = System::Drawing::Color::Green;
				}
				else if (walker.second.commit_number > 0){
					for (int j = 1; j <= 3; j++){
						//this->dataGridView1->Rows[row_number]->Cells[j]->Style->BackColor = System::Drawing::Color::DeepSkyBlue;
					}
					DataGridViewRow ^dataGridViewRow = this->dataGridView1->Rows[row_number];
					dataGridViewRow->HeaderCell->Style->BackColor = System::Drawing::Color::DeepSkyBlue;
				}
				else {
					for (int j = 1; j <= 3; j++){
						//this->dataGridView1->Rows[row_number]->Cells[j]->Style->BackColor = System::Drawing::Color::Red;
					}
					DataGridViewRow ^dataGridViewRow = this->dataGridView1->Rows[row_number];
					dataGridViewRow->HeaderCell->Style->BackColor = System::Drawing::Color::Red;
				}
				i++;
			}
			row_number++;
		}
	}

			 // fill the column with information about version of the program
	private: void fillVersionColumn() {
		int row_number = 0;
		for (auto& walker : data){
			String ^ act = gcnew String(walker.second.commit_info.c_str());
			this->dataGridView1->Rows[row_number]->Cells[4]->Value = act;
			row_number++;
		}
	}

			 // button Fetch
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		if (info.areCredentialsSet() == 1){
			l_paths.clear();
			fillLPaths();

			if ((loaded == 1) && (l_paths.size() > 0)) {
				CommitInfo commit_info;
				info.fillAllFetchData(l_paths);
				data = info.getPackageInfoMap();
				fillVersionColumn();
				highlightRows();

			}
			else if ((loaded != 1) && (l_paths.size() == 0)) {
				SetConsoleTextAttribute(hConsole, 12);
				cerr << "You have to load some repositories and then select which of them you want to fetch" << endl;
				SetConsoleTextAttribute(hConsole, 7);
			}
			else if ((loaded == 1) && (l_paths.size() == 0)) {
				SetConsoleTextAttribute(hConsole, 12);
				cerr << "You have to select repositories you want to fetch" << endl;
				SetConsoleTextAttribute(hConsole, 7);
			}
		}
		else {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to set credentials" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

			 // button output to csv file
	private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
		map <string, PackageInfo> to_output = map <string, PackageInfo>();
		data = map <string, PackageInfo>();
		info.getPackages(origin_local_path);
		data = info.getPackageInfoMap();

		if ((loaded == 1) && (relative_path_first.empty() == false) && (save_csv_there.empty() == false)){		// absolute path
			for (unsigned int j = 0; j < to_fetch.size(); j++) {
				int row_count = this->dataGridView1->Rows->Count;

				for (int i = 0; i < row_count; i++){
					if (i == to_fetch.at(j)){
						String ^ folder = this->dataGridView1->Rows[i]->Cells[1]->Value->ToString();
						string folder_ = msclr::interop::marshal_as<std::string>(folder);

						String ^ url = this->dataGridView1->Rows[i]->Cells[2]->Value->ToString();
						string url_ = msclr::interop::marshal_as<std::string>(url);

						String ^ tag = this->dataGridView1->Rows[i]->Cells[3]->Value->ToString();
						string tag_ = msclr::interop::marshal_as<std::string>(tag);

						PackageInfo* data_info = new PackageInfo[1];
						data_info[0].local_path = folder_;
						data_info[0].url = url_;
						data_info[0].tag = tag_;
						to_output.insert(pair<string, PackageInfo>(data_info[0].local_path, data_info[0]));
						break;
					}
				}
			}

			info.outputToCsv(to_output, relative_path_first, save_csv_there);
			SetConsoleTextAttribute(hConsole, 10);
			cout << "File 'output.csv' saved to : " << save_csv_there << endl;
			SetConsoleTextAttribute(hConsole, 7);
			to_output = map <string, PackageInfo>();
		}
		else if (save_csv_there.empty() == true) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select folder where to save csv file" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (loaded != 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to load some data" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

			 // checkBox column1
	private: System::Void dataGridView1_CellContentClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {

		if (loaded == 1){
			to_fetch.clear();
			for (unsigned int i = 0; i < data.size(); i++)
			{
				if (Convert::ToBoolean(this->dataGridView1->Rows[i]->Cells[0]->EditedFormattedValue) == true)		 // 
				{
					if (std::find(to_fetch.begin(), to_fetch.end(), i) == to_fetch.end()) {
						to_fetch.push_back(i);
						comboBox2->Text = "      No tags/branches loaded";
						comboBox2->Items->Clear();
						version_counter = 1;
						textBox4->Text = "Write tag here or just push button for auto-fill";
						richTextBox1->Text = "Write commit message";
					}
				}
			}
		}
	}

			 // button Select all
	private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {

		if (loaded == 1){
			for (unsigned int i = 0; i < data.size(); i++)
			{
				if (Convert::ToBoolean(this->dataGridView1->Rows[i]->Cells[0]->EditedFormattedValue) == false) {
					this->dataGridView1->Rows[i]->Cells[0]->Value = true;
				}
				to_fetch.push_back(i);
			}
		}
	}

			 // button Discard all
	private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {

		if (loaded == 1){
			for (unsigned int i = 0; i < data.size(); i++)
			{

				if (Convert::ToBoolean(this->dataGridView1->Rows[i]->Cells[0]->EditedFormattedValue) == true) {
					this->dataGridView1->Rows[i]->Cells[0]->Value = false;
				}
				to_fetch.clear();
			}
		}
	}

			 // folder browser dialog to select path to folder 
	private: string selectPath(string descript, int num_of_text_box) {
		String^ description = gcnew String(descript.c_str());
		FolderBrowserDialog ^fdb = gcnew FolderBrowserDialog();
		fdb->RootFolder = System::Environment::SpecialFolder::Desktop;
		fdb->Description = description;
		if (num_of_text_box == 1) fdb->ShowNewFolderButton = false;
		fdb->ShowDialog();

		if (num_of_text_box == 1){
			textBox1->Text = fdb->SelectedPath;
		}
		else if (num_of_text_box == 2) {
			textBox2->Text = fdb->SelectedPath;
		}
		else if (num_of_text_box == 3) {
			textBox7->Text = fdb->SelectedPath;
		}
		else if (num_of_text_box == 4) {
			textBox8->Text = fdb->SelectedPath;
		}
		else if (num_of_text_box == 5) {
			textBox5->Text = fdb->SelectedPath;
		}

		using namespace msclr::interop;
		std::string path(marshal_as<std::string>(fdb->SelectedPath));
		return path;
	}

			 // edit last_searched to an actual version
	private: void fillLastSearched(string path) {
		last_searched.erase(std::remove(last_searched.begin(), last_searched.end(), path), last_searched.end());
		if (path.empty() == false) {
			last_searched.push_back(path);
			local_path = path;
			addToXml(path);
		}
	}

			 // button Select folder
	private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
		loaded = 0;
		string path = selectPath(" \n    Select folder for searching .git repositories    ", 1);
		// absolute_path = path;

		fillLastSearched(path);
		addToComboBox();
	}

			 // text box to display path
	private: System::Void textBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		using namespace msclr::interop;
		std::string path(marshal_as<std::string>(textBox1->Text));
		if (path != "No folder selected") {
			local_path = path;
			//fillLastSearched(path);
			//addToComboBox();
		}
	}

			 // button clear
	private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e) {

		data = map <string, PackageInfo>();
		// to_delete = map <string, PackageInfo>();
		to_clone = map <string, PackageInfo>();
		tag_to_switch = multimap <string, PackageInfo>();
		csv_data = multimap <string, PackageInfo>();

		folder_path.clear();
		to_fetch.clear();
		l_paths.clear();
		csv_paths.clear();
		create_repositories_there.clear();
		tags.clear();
		remote.clear();
		branches.clear();
		comboBox1->Text = "                                             Last searched";
		comboBox2->Items->Clear();
		comboBox2->Text = "      No tags/branches loaded";
		textBox1->Text = "No folder selected";
		textBox2->Text = "No folder selected";
		textBox3->Text = "No file selected";
		textBox4->Text = "Write tag here or just push button for auto-fill";
		textBox5->Text = "No folder selected";
		richTextBox1->Text = "Write commit message";
		textBox6->Text = "";
		textBox7->Text = "No folder selected";
		textBox8->Text = "No folder selected";
		textBox9->Text = "Write remote url here";
		loaded = -1;
		commit_message = "Commit created by button, message box empty";

		path_to_clone.clear();
		path_to_csv.clear();
		save_csv_there.clear();
		tag_from_textb.clear();
		version_counter = 1;
		this->dataGridView1->Rows->Clear();
		this->dataGridView1->Refresh();

	}

			 // combobox na vyber z poslednych hladani
	private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {

		using namespace msclr::interop;
		std::string path(marshal_as<std::string>(comboBox1->Text));

		loaded = 0;

		fillLastSearched(path);
		textBox1->Text = comboBox1->Text;
	}

			 // button to set where to clone remote repository
	private: System::Void button8_Click(System::Object^  sender, System::EventArgs^  e) {
		string path = selectPath(" \n    Select folder to clone remote repositories    ", 2);
		path_to_clone = path;

		fillLastSearched(path_to_clone);
		addToComboBox();
	}

			 // text where to clone
	private: System::Void textBox2_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		using namespace msclr::interop;
		std::string path(marshal_as<std::string>(textBox2->Text));

		if ((path != "No folder selected") && (path != path_to_clone) && (path.empty() == false)) 	path_to_clone = path;
	}

			 // button Clone
	private: System::Void button9_Click(System::Object^  sender, System::EventArgs^  e) {

		if ((path_to_clone.empty() == false) && (path_to_csv.empty() == false)) {
			fillLastSearched(path_to_clone);
			addToComboBox();

			info.inputUrl(path_to_csv);
			info.cloneRepository(path_to_clone);
		}
		else if ((path_to_clone.empty() == false) && (path_to_csv.empty() == true)) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select csv file" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if ((path_to_clone.empty() == true) && (path_to_csv.empty() == false)){
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select folder to clone remote repositories" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if ((path_to_clone.empty() == true) && (path_to_csv.empty() == true)) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select csv file and folder where to clone remote repositories" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

			 // Select folder containing csv file with data
	private: System::Void button10_Click(System::Object^  sender, System::EventArgs^  e) {

		OpenFileDialog ^f = gcnew  OpenFileDialog();
		f->Filter = "CSV|*.csv";
		f->ShowDialog();
		textBox3->Text = f->FileName;
		using namespace msclr::interop;
		std::string path(marshal_as<std::string>(f->FileName));
		path_to_csv = path;
	}

			 // prints selected path of folder with csv file
	private: System::Void textBox3_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		using namespace msclr::interop;
		std::string file_path(marshal_as<std::string>(textBox3->Text));
		if ((file_path != "No file selected") && (file_path != path_to_csv) && (file_path.empty() == false)) path_to_csv = file_path;
	}

			 // create auto-tag on branch master
	private: void createTagOnMaster(string path) {
		stringstream full_tag;
		string item, numb1, numb2, tagg;
		int counter = 0;
		int max1 = 0;
		int max2 = 0;

		for (unsigned int i = 0; i < tags.size(); i++) {
			size_t n = std::count(tags.at(i).begin(), tags.at(i).end(), '.');
			if (n == 1){
				stringstream ful_tag;
				tagg = tags.at(i);
				tagg.erase(0, 10);
				ful_tag.str(tagg);
				while (getline(ful_tag, item, '.')) {

					if (counter == 0) numb1 = item;
					else if (counter == 1) numb2 = item;
					counter++;
				}
				counter = 0;
				int number1 = stoi(numb1);
				int number2 = stoi(numb2);

				if ((number1 >= max1)) {
					if ((number1 == max1) && (number2 >= max2))
					{
						max1 = number1;
						max2 = number2;
					}
					else if (number1 > max1){
						max1 = number1;
						max2 = number2;
					}
				}
			}
		}

		max2++;
		tag_from_csv = to_string(max1) + "." + to_string(max2);
		info.createTag(path, tag_from_csv);
	}

			 // create auto-tag on branch(besides master)
	private: void createTagOnOtherBranch(string path) {

		string item, branch_name, numb, tag_to_switch;
		branch_name = info.getCurrentBranch(path);
		vector<string> tags_in_same_branch;
		int number = 1;
		int max_num = 0;
		size_t count;

		if (branch_name.find(".master") != string::npos) branch_name.erase((branch_name.length() - 6), 6);
		count = std::count(branch_name.begin(), branch_name.end(), '.');

		for (unsigned int i = 0; i < tags.size(); i++) {
			size_t count2 = std::count(tags.at(i).begin(), tags.at(i).end(), '.');
			if ((count == count2) && (tags.at(i).find(branch_name) != std::string::npos)) {

				tags_in_same_branch.push_back(tags.at(i));
			}
		}

		for (unsigned int i = 0; i < tags_in_same_branch.size(); i++){
			tags_in_same_branch.at(i).erase(0, 10);
		}

		for (unsigned int i = 0; i < tags_in_same_branch.size(); i++) {
			stringstream full_tag;
			full_tag.str(tags_in_same_branch.at(i));

			while (getline(full_tag, item, '.')) {
				numb = item;
			}
			number = stoi(numb);
			if (number > max_num) {
				max_num = number;
			}
		}
		max_num++;
		tag_from_csv = branch_name + to_string(max_num);
		info.createTag(path, tag_from_csv);
		tag_to_switch = "refs/tags/" + tag_from_csv;
		tags_in_same_branch.clear();
	}

			 // display tag that was created right now
	private: void displayNewTag(){
		string message = "tag " + tag_from_csv + " created";
		String ^ message_ = gcnew String(message.c_str());
		textBox4->Text = message_;
		String ^ tag = gcnew String(tag_from_csv.c_str());
		for (unsigned int i = 0; i < to_fetch.size(); i++) this->dataGridView1->Rows[to_fetch.at(i)]->Cells[3]->Value = tag;
		tag_from_csv.clear();
	}

			 // button create tag
	private: System::Void button11_Click(System::Object^  sender, System::EventArgs^  e) {

		l_paths.clear();
		fillLPaths();

		if ((tag_from_textb.find("tag") != string::npos) && (tag_from_textb.find("created") != string::npos)) tag_from_textb.clear();
		if ((l_paths.size() == 1) || (tag_from_textb.empty() == false)) {
			stringstream full_tag;
			string path;
			path = l_paths.at(0);

			loadTags();

			// if user write something -> want to use his own tag
			if (tag_from_textb.empty() == false){
				for (unsigned int i = 0; i < l_paths.size(); i++){
					tag_from_csv = tag_from_textb;
					info.createTag(l_paths.at(i), tag_from_csv);
					string tmp = info.getCurrentBranch(l_paths.at(i));
					if (tmp != "master") info.setToTag(l_paths.at(i), tag_from_csv);
				}
			}
			// if user didn't write anything, auto-tag is created
			else {

				string branch = info.getCurrentBranch(path);
				if (branch == "master") {

					// no tags before, create tag 1.0
					if (tags.size() == 0) {
						tag_from_csv = "1.0";
						info.createTag(path, tag_from_csv);
					}
					else {
						createTagOnMaster(path);
					}
				}
				else {
					createTagOnOtherBranch(path);
				}
			}
			displayNewTag();
		}
		else if (l_paths.size() > 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You can create auto-tag to just one repository, if you want to create common tag for more repositories, you have to write the tag " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (l_paths.size() < 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select some repository " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

			 // Write tag here or just push button for auto-fill
	private: System::Void textBox4_TextChanged(System::Object^  sender, System::EventArgs^  e) {

		using namespace msclr::interop;
		std::string message(marshal_as<std::string>(textBox4->Text));
		if ((message != "Write tag here or just push button for auto-fill") && (message.empty() == false)) tag_from_textb = message;
	}

			 // load branches of selected repository and add them to comboBox
	private: void loadBranches() {
		string path;
		path = l_paths.at(0);
		info.loadTags(path);
		branches = info.getBranches();

		for (unsigned int i = 0; i < branches.size(); i++) {
			string branch_name = (branches.at(i));
			if (branch_name.find("refs/heads/") != string::npos){
				branch_name.erase(0, 11);
				String ^ branch = gcnew String(branch_name.c_str());
				comboBox2->Items->Add(branch);
			}
		}
	}

			 // load tags to checkbox2
	private: void loadTags(){
		string path;
		path = l_paths.at(0);

		info.loadTags(path);
		tags = info.getTags();

		// cout << "Tags in : " << path << endl;
		//if (tags.size() == 0) cout << "There are no tags in this repository" << endl;
		/*else {
			for (unsigned int i = 0; i < tags.size(); i++) {
			cout << tags.at(i) << endl;
			}
			cout << endl;
			}*/

		for (unsigned int i = 0; i < tags.size(); i++) {
			string tag_name = (tags.at(i));
			if (tag_name.find("refs/tags/") != string::npos) {
				tag_name.erase(0, 10);
				String ^ tag = gcnew String(tag_name.c_str());
				comboBox2->Items->Add(tag);
			}
		}
	}

			 // button load tags / branches
	private: System::Void button13_Click(System::Object^  sender, System::EventArgs^  e) {
		l_paths.clear();
		fillLPaths();

		if ((l_paths.size() == 1)) {

			comboBox2->Items->Clear();
			loadBranches();
			loadTags();

			comboBox2->Text = "Select tag/branch";
		}
		else if (l_paths.size() > 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You can see tags from just one repository " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (l_paths.size() < 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select some repository " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

			 // button switch to tag / branch
	private: System::Void button12_Click(System::Object^  sender, System::EventArgs^  e) {

		l_paths.clear();
		fillLPaths();

		if ((version.empty() == false) && (l_paths.size() == 1)) {

			if (0 == info.gitDiff(l_paths.at(0), 0)) {
				can_continue = 1;
				if ((version.find("refs/tags/") == std::string::npos) && (version.find("tags/") == std::string::npos)) tag_from_csv = version;
				else if (version.find("refs/tags/") != std::string::npos) version.erase(0, 10);
				else if (version.find("tags/") != std::string::npos) version.erase(0, 5);

				version_counter = 1;
				if (version.find("master") == string::npos){
					version = "refs/tags/" + version;
				}
				else version = "refs/heads/" + version;
				info.setToTag(l_paths.at(0), version);

				for (auto& kv : data) {
					if (kv.second.local_path == l_paths.at(0)){
						kv.second.tag = tag_from_csv;
						// if (kv.second.tag=="heads/master") 
					}
				}
				String ^ tag = gcnew String(tag_from_csv.c_str());
				this->dataGridView1->Rows[to_fetch.at(0)]->Cells[3]->Value = tag;
			}
		}

		else if (l_paths.size() > 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Too much repositories selected " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (l_paths.size() < 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select some repository " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (version.empty()) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Choose some version " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

			 // comboBox no tags loaded
	private: System::Void comboBox2_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		using namespace msclr::interop;
		std::string tag(marshal_as<std::string>(comboBox2->Text));
		version = tag;
	}

			 // display url to remote repo
	private: System::Void textBox5_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	}

	private: System::Void textBox6_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	}

			 // Add all changes and create commit
	private: System::Void button14_Click(System::Object^  sender, System::EventArgs^  e) {
		string result;

		l_paths.clear();
		fillLPaths();
		if (l_paths.size() < 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select some repository " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else {
			for (unsigned int i = 0; i < l_paths.size(); i++) {
				if (0 == info.addAll(l_paths.at(i), commit_message)) {
					richTextBox1->Text = "Commit was created successfully";
					highlightUncommitted();
				}
				else {
					richTextBox1->Text = "Problem creating commit";
					SetConsoleTextAttribute(hConsole, 12);
					cerr << "Problem creating commit, repository : " << l_paths.at(i) << endl;
					SetConsoleTextAttribute(hConsole, 7);
				}

				result = info.gitDescribe(l_paths.at(i));
				if (result.find("refs/heads/") != string::npos) result.erase(0, 11);
				else if (result.find("heads/") != string::npos) result.erase(0, 6);
				String ^ tag = gcnew String(result.c_str());
				this->dataGridView1->Rows[to_fetch.at(i)]->Cells[3]->Value = tag;
			}
		}
	}

			 // button Push
	private: System::Void button15_Click(System::Object^  sender, System::EventArgs^  e) {
		vector<string> to_push;

		l_paths.clear();
		fillLPaths();

		if (l_paths.size() < 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select some repository " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else {

			CommitInfo commit_info;
			info.fillAllFetchData(l_paths);
			data = info.getPackageInfoMap();

			for (unsigned int i = 0; i < l_paths.size(); i++){

				for (auto& walker : data){

					if ((walker.second.local_path == l_paths.at(i)) && (walker.second.commit_number > 0)) to_push.push_back(l_paths.at(i));
					else if ((walker.second.local_path == l_paths.at(i)) && (walker.second.commit_number == 0)) {
						SetConsoleTextAttribute(hConsole, 12);
						cerr << "Cannot push -> you have an actual version ! (repository : " << l_paths.at(i) << ")" << endl;
						SetConsoleTextAttribute(hConsole, 7);
					}
					else if ((walker.second.local_path == l_paths.at(i)) && (walker.second.commit_number < 0)) {
						SetConsoleTextAttribute(hConsole, 12);
						cerr << "Cannot push -> you are behind remote ! (repository : " << l_paths.at(i) << ")" << endl;
						SetConsoleTextAttribute(hConsole, 7);
					}
				}
			}
			info.performPushInThread(to_push);
		}
	}

			 // Select where to save csv file
	private: System::Void button16_Click(System::Object^  sender, System::EventArgs^  e) {
		save_csv_there = selectPath(" \n    Select folder to save output.csv    ", 3);
		origin_local_path = local_path;

		fillLastSearched(save_csv_there);
		addToComboBox();
	}

			 // display selected path where to save csv file
	private: System::Void textBox7_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		using namespace msclr::interop;
		std::string path(marshal_as<std::string>(textBox7->Text));
		if ((path != "No folder selected") && (path != save_csv_there)) save_csv_there = path;
	}

			 // force switch
	private: System::Void button17_Click(System::Object^  sender, System::EventArgs^  e) {

		l_paths.clear();
		fillLPaths();

		if ((version.empty() == false) && (l_paths.size() == 1)) {

			if ((version.find("refs/tags/") == std::string::npos) && (version.find("tags/") == std::string::npos)) tag_from_csv = version;
			else if (version.find("refs/tags/") != std::string::npos) version.erase(0, 10);
			else if (version.find("tags/") != std::string::npos) version.erase(0, 5);
			version_counter = 1;
			if (version.find("master") == string::npos){
				version = "refs/tags/" + version;
			}
			else version = "refs/heads/" + version;

			info.setToTag(l_paths.at(0), version);

			for (auto& kv : data) {
				if (kv.second.local_path == l_paths.at(0)){
					kv.second.tag = tag_from_csv;
					// if (kv.second.tag=="heads/master") 
				}
			}
			String ^ tag = gcnew String(tag_from_csv.c_str());
			this->dataGridView1->Rows[to_fetch.at(0)]->Cells[3]->Value = tag;
			highlightUncommitted();
		}

		else if (l_paths.size() > 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Too much repositories selected " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (l_paths.size() < 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select some repository " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (version.empty()) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Choose some version " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

			 // show conflict files
	private: System::Void button18_Click(System::Object^  sender, System::EventArgs^  e) {
		int result;

		l_paths.clear();
		fillLPaths();

		if (l_paths.size() == 1) {
			result = info.gitDiff(l_paths.at(0), 3);
			if (result == 0) {
				SetConsoleTextAttribute(hConsole, 12);
				cout << "There are no conflict files in this repository. ( repo : " << l_paths.at(0) << " )" << endl;
				SetConsoleTextAttribute(hConsole, 7);
			}
		}
		else if (l_paths.size() > 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Too much repositories selected " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (l_paths.size() < 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select some repository " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

			 // Set credentials
	private: System::Void button19_Click(System::Object^  sender, System::EventArgs^  e) {

		info.setCredentials();

		button19->BackColor = System::Drawing::Color::DarkSeaGreen;
		button19->Text = "Set different credentials";
	}

			 // select folder withou git repositories
	private: System::Void button20_Click(System::Object^  sender, System::EventArgs^  e) {

		string path = selectPath("Select folder without git repositories(git repo is going to be initialized in each subfolder of selected folder)    ", 4);
		create_repositories_there = path;

		fillLastSearched(path);
		addToComboBox();

	}

			 // create git repo in each subfolder
	private: System::Void button21_Click(System::Object^  sender, System::EventArgs^  e) {
		unsigned int counter = 0;

		if (create_repositories_there.empty() == false) {

			vector<string> subfolders;
			subfolders = info.findSubfold(create_repositories_there);

			for (unsigned int i = 0; i < subfolders.size(); i++) {
				counter += info.initializeRepo(subfolders.at(i));
			}
			if (counter == 0) {

				SetConsoleTextAttribute(hConsole, 10);
				cout << "Git repository was created in each subfolder of selected folder. (" << subfolders.size() << " repositories)" << endl;
				SetConsoleTextAttribute(hConsole, 7);
			}
			else if (counter <= subfolders.size()) {
				SetConsoleTextAttribute(hConsole, 12);
				cout << "Git repository was created in " << (subfolders.size() - counter) << " subfolders. Total number of subfolders : " << subfolders.size() << endl;
				SetConsoleTextAttribute(hConsole, 7);
			}
		}
		else{
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select some folder" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

	private: System::Void textBox9_TextChanged(System::Object^  sender, System::EventArgs^  e) {

		using namespace msclr::interop;
		std::string url(marshal_as<std::string>(textBox9->Text));
		if ((url != "Write remote url here") && (url != "Remote url set successfully") && (url != "Remote URL added just to repositories without remote") && (url != "You have to set credentials!") && (url.empty() == false)) remote = url;
	}

			 // add a remote
	private: System::Void button22_Click(System::Object^  sender, System::EventArgs^  e) {
		unsigned int counter = 0;
		unsigned int counter_ = 0;
		l_paths.clear();
		fillLPaths();

		if ((remote.empty() == false) && (l_paths.size() > 0)) {

			for (unsigned int i = 0; i < l_paths.size(); i++)  {
				string result = info.addRemoteOrigin(l_paths.at(i), remote);
				if (result == "already_has_remote") counter++;
				else if (result.empty() == false) {
					String ^ url = gcnew String(result.c_str());
					this->dataGridView1->Rows[to_fetch.at(i)]->Cells[2]->Value = url;
				}
				else if (result.empty() == true) counter_++;
			}

			if ((counter == 0) && (counter_ == 0)) {
				textBox9->Text = "Remote url set successfully for all selected repositories";
				SetConsoleTextAttribute(hConsole, 10);
				cout << endl << endl << "All remotes added" << endl;
				SetConsoleTextAttribute(hConsole, 7);
			}
			else if (counter <= l_paths.size()) {
				if ((l_paths.size() - counter - counter_) > 0) textBox9->Text = "Remote URL added just to repositories without remote";
				else if ((l_paths.size() - counter_) == 0) textBox9->Text = "You have to set credentials!";
				SetConsoleTextAttribute(hConsole, 12);
				cout << endl << endl << "Remote added to " << (l_paths.size() - counter - counter_) << " repositories. Total number of repositories : " << l_paths.size() << endl;
				SetConsoleTextAttribute(hConsole, 7);
			}

		}
		else if ((remote.empty() == false) && (l_paths.size() <= 0)) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select repositories to add a remote" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if ((remote.empty() == true) && (l_paths.size() > 0)) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to write your remote url" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if ((remote.empty() == true) && (l_paths.size() <= 0)) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select repositories to add a remote and write your remote url" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}

	}

	private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void groupBox4_Enter(System::Object^  sender, System::EventArgs^  e) {
	}


	private: System::Void richTextBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) {

		using namespace msclr::interop;
		std::string message(marshal_as<std::string>(richTextBox1->Text));
		if ((message != "Write commit message") && (message != "Commit was created successfully")) commit_message = message;
	}


	private: System::Void textBox8_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		using namespace msclr::interop;
		std::string path(marshal_as<std::string>(textBox8->Text));
		if ((path != "No folder selected") && (path != create_repositories_there)) create_repositories_there = path;
	}

			 // return number of digits of sent number
	private: unsigned getNumberOfDigits(unsigned i)
	{
		return i > 0 ? (int)log10((double)i) + 1 : 1;
	}

			 // find "biggest" or "latest" tag from sent tags
	private: void findBiggestTag(vector<string> conflict_tags) {
		vector<string> specific_tag;
		string latest_tag, item, numb1, numb2;

		for (unsigned int j = 0; j < conflict_tags.size(); j++){

			// load tags from csv files
			for (auto& walker : tag_to_switch){
				if (walker.second.local_path.find(conflict_tags.at(j)) != string::npos)	specific_tag.push_back(walker.second.tag);
			}

			// if you have less then 2 tags -> break
			if (specific_tag.size() < 2) break;
			latest_tag = specific_tag.at(0);

			for (unsigned int i = 1; i < specific_tag.size(); i++){

				string tag__ = latest_tag;
				size_t found = tag__.find(".");
				tag__.erase(remove(tag__.begin() + found, tag__.end(), '.'), tag__.end());
				double latest_tag_num = stod(tag__);

				// find the biggest
				size_t n = count(specific_tag.at(i).begin(), specific_tag.at(i).end(), '.');
				string str = specific_tag.at(i);

				if (n > 1) {

					str.erase(remove(str.begin(), str.end(), '.'), str.end());
				}

				double tag_number = stod(str);
				double numb1 = getNumberOfDigits(tag_number);
				double numb2 = getNumberOfDigits(latest_tag_num);

				if (numb1 > numb2) {
					for (int i = 0; i < numb1 - numb2; i++) tag__ += "0";

					latest_tag_num = stod(tag__);
				}
				else if (numb1 < numb2){
					for (int i = 0; i < numb2 - numb1; i++) str += "0";

					tag_number = stod(str);
				}

				if (tag_number > latest_tag_num) latest_tag = specific_tag.at(i);

			}

			// write the biggest
			for (auto& walker : tag_to_switch){
				if (walker.second.local_path.find(conflict_tags.at(j)) != string::npos) {
					size_t found1 = walker.second.tag.find(".");
					size_t found2 = latest_tag.find(".");
					if (found2 >= found1) walker.second.tag = latest_tag;
				}
			}

			// clear
			latest_tag.clear();
			specific_tag.clear();
		}
	}

			 // highlight result of compare operation, files that are not changed are highlighted in green, files that are going to be cloned are highlighted in blue, 
			 // files that are not contained in csv file are highlighted in gray, files with different tags have their tag/branch column highlighted in red - also " old_tag -> new_tag " is printed
	private: void highlightComparisonResult() {
		int row_count = this->dataGridView1->Rows->Count;
		vector<string> conflict_tags;
		for (auto& walker : ok_files) {

			for (int i = 0; i < row_count; i++){
				String ^ folder = this->dataGridView1->Rows[i]->Cells[1]->Value->ToString();
				string folder_ = msclr::interop::marshal_as<std::string>(folder);

				String ^ url = this->dataGridView1->Rows[i]->Cells[2]->Value->ToString();
				string url_ = msclr::interop::marshal_as<std::string>(url);

				if ((walker.second.local_path.find(folder_) != string::npos) && (url_ == walker.second.url)){
					for (int j = 1; j < 4; j++) this->dataGridView1->Rows[i]->Cells[j]->Style->ForeColor = System::Drawing::Color::Green;

					break;
				}

			}
		}

		for (auto& walker : to_delete) {
			for (int i = 0; i < row_count; i++){
				String ^ folder = this->dataGridView1->Rows[i]->Cells[1]->Value->ToString();
				string folder_ = msclr::interop::marshal_as<std::string>(folder);

				String ^ url = this->dataGridView1->Rows[i]->Cells[2]->Value->ToString();
				string url_ = msclr::interop::marshal_as<std::string>(url);

				if ((walker.second.local_path.find(folder_) != string::npos) && (url_ == walker.second.url)){
					for (int j = 1; j < 4; j++) this->dataGridView1->Rows[i]->Cells[j]->Style->ForeColor = System::Drawing::Color::LightGray;
					break;
				}
			}
		}

		for (auto& walker : tag_to_switch) {

			for (int i = 0; i < row_count; i++){
				String ^ folder = this->dataGridView1->Rows[i]->Cells[1]->Value->ToString();
				string folder_ = msclr::interop::marshal_as<std::string>(folder);

				String ^ url = this->dataGridView1->Rows[i]->Cells[2]->Value->ToString();
				string url_ = msclr::interop::marshal_as<std::string>(url);

				String ^ tag = this->dataGridView1->Rows[i]->Cells[3]->Value->ToString();
				string tag_ = msclr::interop::marshal_as<std::string>(tag);

				if ((walker.second.local_path.find(folder_) != string::npos) && (url_ == walker.second.url)){

					if (tag_.find("->") != string::npos) conflict_tags.push_back(folder_);
					tag_ += " -> " + walker.second.tag;

					String ^ tag_change = gcnew String(tag_.c_str());
					this->dataGridView1->Rows[i]->Cells[3]->Value = tag_change;

					this->dataGridView1->Rows[i]->Cells[3]->Style->ForeColor = System::Drawing::Color::Red;
					break;
				}
			}
		}

		findBiggestTag(conflict_tags);

		for (auto& walker : to_clone) {

			String ^ path = gcnew String(walker.second.local_path.c_str());
			String ^ url = gcnew String(walker.second.url.c_str());
			String ^ tag = gcnew String(walker.second.tag.c_str());

			int index = this->dataGridView1->Rows->Add();
			this->dataGridView1->Rows[index]->Cells[1]->Value = path;
			this->dataGridView1->Rows[index]->Cells[2]->Value = url;
			this->dataGridView1->Rows[index]->Cells[3]->Value = tag;
			for (int j = 1; j < 4; j++) this->dataGridView1->Rows[index]->Cells[j]->Style->ForeColor = System::Drawing::Color::DeepSkyBlue;

		}
	}

			 // print comparison result to console
	private: void showComparisonResult() {

		if ((/*to_delete.size() +*/ to_clone.size() + tag_to_switch.size()) > 0) {

			SetConsoleTextAttribute(hConsole, 14);
			cout << "Changes that will be applied after pushing button 'Apply changes' (source csv " << path_to_csv << ")" << endl << endl;
			SetConsoleTextAttribute(hConsole, 7);

			SetConsoleTextAttribute(hConsole, 10);
			cout << "Files that won't be changed : " << endl;
			SetConsoleTextAttribute(hConsole, 7);
			if (ok_files.size() == 0) cout << "No files" << endl;
			else for (auto& walker : ok_files)	cout << walker.second.local_path << "		" << walker.second.url << "			" << walker.second.tag << endl;
			cout << endl << endl;

			SetConsoleTextAttribute(hConsole, 10);
			cout << "Files to be cloned : " << endl;
			SetConsoleTextAttribute(hConsole, 7);
			if (to_clone.size() == 0) cout << "No files" << endl;
			else for (auto& walker : to_clone)	cout << walker.second.local_path << "		" << walker.second.url << "			" << walker.second.tag << endl;
			cout << endl << endl;

			SetConsoleTextAttribute(hConsole, 10);
			cout << "Files to be changed(these files differs in tags) : " << endl;
			SetConsoleTextAttribute(hConsole, 7);
			if (tag_to_switch.size() == 0) cout << "No files" << endl;
			else for (auto& walker : tag_to_switch)	cout << walker.second.local_path << "		" << walker.second.url << "			" << walker.second.tag << endl;
			cout << endl << endl;

			SetConsoleTextAttribute(hConsole, 10);
			cout << "Files not included in csv : " << endl;
			SetConsoleTextAttribute(hConsole, 7);
			if (to_delete.size() == 0) cout << "No files" << endl;
			else for (auto& walker : to_delete)	cout << walker.second.local_path << "		" << walker.second.url << "			" << walker.second.tag << endl;
			cout << endl << endl;
		}
		else {
			SetConsoleTextAttribute(hConsole, 10);
			cout << "There is no difference between csv file and your grid " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		compared = 1;

		highlightComparisonResult();
	}

			 //divide multimap according to data
	private: void divideCsvMapAccordingToData(multimap <string, PackageInfo> csv_data) {

		int not_found = 0;
		ok_files = map <string, PackageInfo>();
		to_clone = map <string, PackageInfo>();
		tag_to_switch = multimap <string, PackageInfo>();
		to_delete = map <string, PackageInfo>();

		to_delete = data;

		for (auto& walker : csv_data)	{
			for (auto& walker2 : data){

				if ((walker2.second.local_path.find(walker.second.local_path) != string::npos) && (walker2.second.url == walker.second.url) && (walker2.second.tag == walker.second.tag)){
					PackageInfo* data_info = new PackageInfo[1];
					data_info[0].local_path = walker.second.local_path;
					data_info[0].url = walker.second.url;
					data_info[0].tag = walker.second.tag;
					ok_files.insert(pair<string, PackageInfo>(data_info[0].local_path, data_info[0]));
					break;
				}

				// everything ok apart from tag
				else if ((walker2.second.local_path.find(walker.second.local_path) != string::npos) && (walker2.second.url == walker.second.url) && (walker2.second.tag != walker.second.tag)){
					PackageInfo* data_info = new PackageInfo[1];
					data_info[0].local_path = walker.second.local_path;
					data_info[0].url = walker.second.url;
					data_info[0].tag = walker.second.tag;
					tag_to_switch.insert(pair<string, PackageInfo>(data_info[0].local_path, data_info[0]));
					break;
				}

				// unknown files(have to be cloned) 
				else if ((walker2.second.local_path.find(walker.second.local_path) == string::npos) || (walker2.second.url != walker.second.url)) {
					not_found++;
				}
			}

			if (not_found == data.size()) {
				PackageInfo* data_info = new PackageInfo[1];
				data_info[0].local_path = walker.second.local_path;
				data_info[0].url = walker.second.url;
				data_info[0].tag = walker.second.tag;
				to_clone.insert(pair<string, PackageInfo>(data_info[0].local_path, data_info[0]));
				// break;
			}
			not_found = 0;
		}

		for (auto& walker : csv_data)	{
			string full_path = relative_path_first + walker.first + "/";
			to_delete.erase(full_path);
		}
		showComparisonResult();
	}

			 // Compare csv with grid
	private: System::Void button23_Click(System::Object^  sender, System::EventArgs^  e) {

		if (path_to_csv.empty() == false) csv_paths.push_back(path_to_csv);

		if ((csv_paths.empty() == false) && (loaded == 1)) {

			for (unsigned int i = 0; i < csv_paths.size(); i++){

				ifstream my_file;
				string one_line, one_box;
				int num_of_lines = 0;

				my_file.open(csv_paths.at(i).c_str());
				if (my_file.is_open()) {
					while (getline(my_file, one_line, '\n')) {
						num_of_lines++;
					}
					my_file.close();
				}

				PackageInfo* csv_data_info = new PackageInfo[num_of_lines];

				my_file.open(csv_paths.at(i).c_str());
				if (my_file.is_open()) {


					int size = 0;
					int counter = 0;
					while (getline(my_file, one_line, '\n')) {
						stringstream line;
						line.str(one_line);

						while (getline(line, one_box, ';'))
						{
							if (counter == 0) csv_data_info[size].local_path = one_box;
							else if (counter == 1) csv_data_info[size].url = one_box;
							else if (counter == 2) csv_data_info[size].tag = one_box;

							counter++;
							if (counter == 3) counter = 0;
						}
						csv_data.insert(pair<string, PackageInfo>(csv_data_info[size].local_path, csv_data_info[size]));
						size++;

					}
					my_file.close();
				}
			}
			divideCsvMapAccordingToData(csv_data);
		}
		else if ((csv_paths.empty() == true) && (loaded != 1)) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Select csv file and load some data" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (csv_paths.empty() == true) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Select some csv to compare" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (loaded != 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Load some data to compare with csv" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

			 // apply changes button
	private: System::Void button24_Click(System::Object^  sender, System::EventArgs^  e) {
		if ((info.areCredentialsSet() == 1) || (to_clone.size() == 0)){
			if ((compared == 1) && ((/*to_delete.size() +*/ to_clone.size() + tag_to_switch.size()) > 0)) {

				/*if (to_delete.size() != 0){
					for (auto& walker : to_delete)	{
					string command = "\"rm -rf " + walker.second.local_path + "\"";
					system(command.c_str());

					SetConsoleTextAttribute(hConsole, 10);
					cout << "Folder " << walker.second.local_path << " was deleted" << endl;
					SetConsoleTextAttribute(hConsole, 7);
					}

					}*/

				if (tag_to_switch.size() != 0) {

					// fetch before switching to tag -> some new tags may be uploaded and csv can include them -> switch to unknown tag will not work -> but after fetch, all tags are downloaded
					l_paths.clear();
					fillLPaths();
					info.fillAllFetchData(l_paths);

					for (auto& walker1 : tag_to_switch)	{

						string full_tag;
						string full_path = relative_path_first + walker1.second.local_path;
						replace(full_path.begin(), full_path.end(), '/', '\\');
						if (walker1.second.tag.find("master") == string::npos) full_tag = "refs/tags/" + walker1.second.tag;
						else full_tag = "refs/heads/" + walker1.second.tag;
						info.setToTag(full_path, full_tag);

						/*
						int row_count = this->dataGridView1->Rows->Count;
						for (int i = 1; i <= row_count; i++){
						String ^ folder = this->dataGridView1->Rows[i]->Cells[1]->Value->ToString();
						string folder_ = msclr::interop::marshal_as<std::string>(folder);
						if (folder_ == walker.second.local_path){
						String ^ tag = gcnew String(walker.second.tag.c_str());
						this->dataGridView1->Rows[i]->Cells[3]->Value = tag;
						break;
						}

						}*/
						SetConsoleTextAttribute(hConsole, 10);
						cout << full_path << " successfully checkouted to " << walker1.second.tag << endl;
						SetConsoleTextAttribute(hConsole, 7);
					}
					tag_to_switch = multimap <string, PackageInfo>();
				}

				if (to_clone.size() != 0){
					for (auto& walker : to_clone)	{
						info.fillDataToClone(walker.second.local_path, walker.second.url, walker.second.tag);
					}
					string path_clone = relative_path_first.erase(relative_path_first.size() - 1, 1);
					replace(path_clone.begin(), path_clone.end(), '/', '\\');
					info.cloneRepository(path_clone);
				}

				/*if (to_delete.size() != 0){
					for (auto& walker : to_delete)	{
					string command = "\"rm -rf " + walker.second.local_path + "\"";
					system(command.c_str());

					SetConsoleTextAttribute(hConsole, 10);
					cout << "Folder " << walker.second.local_path << " was deleted" << endl;
					SetConsoleTextAttribute(hConsole, 7);
					}
					to_delete = map <string, PackageInfo>();
					}
					*/

				SetConsoleTextAttribute(hConsole, 10);
				cout << "All changes applied successfully" << endl;
				SetConsoleTextAttribute(hConsole, 7);

				string path = local_path;
				button7->PerformClick();
				local_path = path;
				loaded = 0;
				path.clear();
				button1->PerformClick();
			}
			else if (compared != 1) {
				SetConsoleTextAttribute(hConsole, 12);
				cerr << "You have to compare data from grid with csv first!" << endl;
				SetConsoleTextAttribute(hConsole, 7);
			}
			else if ((/*to_delete.size() + */ to_clone.size() + tag_to_switch.size()) <= 0) {
				SetConsoleTextAttribute(hConsole, 12);
				cerr << "There are no changes to be applied." << endl;
				SetConsoleTextAttribute(hConsole, 7);
			}
		}
		else {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to set credentials first(clone requires connection to remote)" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

	private: System::Void textBox9_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
		textBox9->Text = "";
	}

	private: System::Void richTextBox1_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
		richTextBox1->Text = "";
	}


	private: System::Void textBox4_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
		textBox4->Text = "";
	}
			 //Select destination folder
	private: System::Void button26_Click(System::Object^  sender, System::EventArgs^  e) {
		string path = selectPath("Select destination folder(this folder will be compared with files in csv)", 5);
		// absolute_path = path;

		fillLastSearched(path);
		addToComboBox();
	}

			 // Switch csv with all files inside
	private: System::Void button25_Click(System::Object^  sender, System::EventArgs^  e) {

		if ((folder_path.empty() == false) && (version.empty() == false) && (l_paths.size() == 1)) {

			string path = folder_path;
			string csv_path = l_paths.at(0) + "\\output.csv";
			// switch to tag
			button12->PerformClick();

			if (can_continue == 1){
				// clear
				button7->PerformClick();
				local_path = path;
				loaded = 0;
				path.clear();

				// load
				button1->PerformClick();

				path_to_csv = csv_path;

				// compare
				button23->PerformClick();
			}

		}
		else if (folder_path.empty() == true) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << " You have to select destination folder(this folder will be compared with files in csv) " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (l_paths.size() > 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Too much repositories selected " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (l_paths.size() < 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select some repository " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (version.empty()) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Choose some version " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}

	}

	private: System::Void textBox5_TextChanged_1(System::Object^  sender, System::EventArgs^  e) {
		using namespace msclr::interop;
		std::string path(marshal_as<std::string>(textBox5->Text));
		if (path != "No folder selected") folder_path = path;

	}

			 // switch all csv including projects
	private: System::Void button27_Click(System::Object^  sender, System::EventArgs^  e) {

		if ((folder_path.empty() == false) && (version.empty() == false) && (l_paths.size() == 1)) {

			vector<string> subfolders;
			string path = folder_path;

			string path_to_folder = l_paths.at(0);

			// switch to tag
			button12->PerformClick();

			if (can_continue == 1){

				subfolders = info.findSubfold(path_to_folder);

				for (unsigned int i = 0; i < subfolders.size(); i++) subfolders.at(i) += "\\output.csv";

				// clear
				button7->PerformClick();
				local_path = path;
				loaded = 0;

				// fill csv paths
				for (unsigned int i = 0; i < subfolders.size(); i++) csv_paths.push_back(subfolders.at(i));

				// load
				button1->PerformClick();

				// compare
				button23->PerformClick();

				// apply changes
				// button24->PerformClick();

				//info.showCsvConflicts(csv_data);

				SetConsoleTextAttribute(hConsole, 10);
				cerr << endl << " All csv files changed  " << endl;
				SetConsoleTextAttribute(hConsole, 7);
			}

		}
		else if (folder_path.empty() == true) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << " You have to select destination folder(this folder will be compared with files in csv) " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (l_paths.size() > 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Too much repositories selected " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (l_paths.size() < 1) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to select some repository " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (version.empty()) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Choose some version " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}

	}

	};
}
